"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var userSchema = new mongoose_1.default.Schema({
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 2 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name must have more than two characters and may only contain letters'
        }
    },
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 2 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Last name must have more than two characters and may only contain letters'
        }
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && value.includes("@");
            },
            message: 'Email is not valid'
        }
    },
    password: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 5;
            },
            message: 'Password is not valid'
        }
    },
    invitedBy: {
        type: String,
        required: false,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'invitedBy is not valid'
        }
    },
}, { timestamps: true });
userSchema.virtual('fullName')
    .get(function () {
    return this.firstName + ' ' + this.lastName;
}).set(function (fullName) {
    var _a = fullName.split(' '), firstName = _a[0], lastName = _a[1];
    this.firstName = firstName;
    this.lastName = lastName;
});
exports.User = mongoose_1.default.model('user', userSchema);
