"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var sessionSchema = new mongoose_1.default.Schema({
    sessionID: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: function (value) {
                return value.length === 64;
            },
            message: 'Session id is invalid'
        }
    },
    userID: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'User id is not valid'
        }
    },
}, { timestamps: true });
exports.Session = mongoose_1.default.model('sessions', sessionSchema);
