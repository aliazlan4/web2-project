"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var fileSchema = new mongoose_1.default.Schema({
    fileName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'File name is not valid'
        }
    },
    ipfsID: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'IPFS id is not valid'
        }
    },
    password_sharing: {
        type: Boolean,
        required: true,
        default: false,
        validate: {
            validator: function (value) {
                return value === true || value === false;
            },
            message: 'Password sharing is not valid'
        }
    },
    password: {
        type: String,
        required: false,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'Password is not valid'
        }
    },
    userID: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'User id is not valid'
        }
    },
}, { timestamps: true });
exports.File = mongoose_1.default.model('files', fileSchema);
