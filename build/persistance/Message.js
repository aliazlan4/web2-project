"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var messageSchema = new mongoose_1.default.Schema({
    fileID: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'File id is not valid'
        }
    },
    message: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0;
            },
            message: 'Message is not valid'
        }
    },
}, { timestamps: true });
exports.Message = mongoose_1.default.model('messages', messageSchema);
