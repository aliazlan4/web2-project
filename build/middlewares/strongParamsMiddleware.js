"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function strongParamsMiddleware(params) {
    return function (request, response, next) {
        var strongParams = {};
        for (var key in params) {
            var type = params[key];
            if (request.query && request.query[key] !== undefined) {
                if (typeof request.query[key] === type) {
                    strongParams[key] = request.query[key];
                }
            }
            else if (request.body && request.body[key] !== undefined) {
                if (typeof request.body[key] === type) {
                    strongParams[key] = request.body[key];
                }
            }
        }
        request.body = null;
        request.query = null;
        response.locals.strongParams = strongParams;
        return next();
    };
}
exports.strongParamsMiddleware = strongParamsMiddleware;
