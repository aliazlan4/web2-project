"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Middleware that authenticates a user if they have a special cookie.
 *
 * Note: This is terrible 'security', do not use this for anything other than entertainment.
 */
function sessionMiddleware(_a) {
    var _b = _a.requireCookie, requireCookie = _b === void 0 ? false : _b;
    // The middleware is configurable so return a function from a function
    return function (request, response, next) {
        var _a;
        // Does the signed 'session' exist? If so add it to the response locals
        if ((_a = request.signedCookies) === null || _a === void 0 ? void 0 : _a.session) {
            var session = request.signedCookies.session;
            response.locals.session = session;
            return next();
        }
        else {
            // No cookie was found, is it even required? If so respond with a 403 and throw an error
            if (requireCookie) {
                response.sendStatus(403);
                return next(new Error('Cookie was required for request but no cookie was found'));
            }
            return next();
        }
    };
}
exports.sessionMiddleware = sessionMiddleware;
