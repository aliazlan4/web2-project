
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const {User} = require('../build/persistance/User');

let SALT = null;

describe('User model tests', function() {

    before(async function(){
		await Mongoose.connect('mongodb://localhost:27017/web2Project-test', { useNewUrlParser: true });
		SALT = await bcrypt.genSalt(10);
    });

    beforeEach(async function() {
        await User.deleteMany({});
    });

    describe('Full name virtual attribute tests', function() {
        it('Should allow setting a full name virtual', async function() {
			const password = await bcrypt.hash("123456", SALT);

			const user = await new User({ firstName: 'FIRST_NAME', lastName: 'LAST_NAME', email: 'hello@domain.com', password: password });

            expect(user.firstName).to.equal('FIRST_NAME');
            expect(user.lastName).to.equal('LAST_NAME');

            user.fullName = 'John Doe';

			expect(user.firstName).to.equal('John');
			expect(user.lastName).to.equal('Doe');

            const savedUser = await user.save();

			expect(savedUser.firstName).to.equal('John');
			expect(savedUser.lastName).to.equal('Doe');
        });
    });

    describe('Email validation tests', function() {
        it('Email without @ symbol', async function() {
			try {
				const password = await bcrypt.hash("123456", SALT);

				const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hellodomain.com', password: password });
				await user.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('users validation failed: email: Email is not valid');
			}
        });

        it('Email unique test', async function() {
			try {
				const password = await bcrypt.hash("123456", SALT);

				const user1 = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: password });
				await user1.save();
				const user2 = await new User({ firstName: 'John', lastName: 'Dow', email: 'hello@domain.com', password: password });
				await user2.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.includes('duplicate key error collection');
			}
        });
    });

    describe('Password validation tests', function() {
        it('Password required validation', async function() {
			try {
				const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: null });
				await user.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('users validation failed: password: Path `password` is required.');
			}
        });

        it('Password bcrypt validation', async function() {
			try {
				const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: "123456" });
				await user.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('users validation failed: password: Password is not valid');
			}
        });
    });

    describe('Timestamps presensce test', function() {
        it('createdAt timestamp presence check', async function() {
			const password = await bcrypt.hash("123456", SALT);

			const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: password });
			await user.save();

			expect(user.createdAt).to.not.equal(undefined);
        });

        it('updatedAt timestamp presence check', async function() {
			const password = await bcrypt.hash("123456", SALT);

			const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: password });
			await user.save();

			expect(user.updatedAt).to.not.equal(undefined);
        });
    });
	
});
