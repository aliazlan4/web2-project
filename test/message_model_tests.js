
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const { Message } = require('../build/persistance/Message');

const userCredentials = {
	email: 'sponge@bob.com',
	password: 'garyTheSnail'
}

describe('Message model tests', function() {

    before(async function(){
		await Mongoose.connect('mongodb://localhost:27017/web2Project-test', { useNewUrlParser: true });
    });

    beforeEach(async function() {
		await Message.deleteMany({});
    });

    describe('message text tests', function() {
		it('Check message length', async function() {
			try {
				const message = await new Message({ message: '', fileID: 'fileID' });
				await message.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('messages validation failed: message: Path `message` is required.');
			}
        });
    });

    describe('fileID tests', function() {
		it('Check fileID length', async function() {
			try {
				const message = await new Message({ message: 'abcd', fileID: '' });
				await message.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('messages validation failed: fileID: Path `fileID` is required.');
			}
        });
    });

	describe('Timestamps presensce test', function () {
		it('createdAt timestamp presence check', async function () {
			const message = await new Message({ message: 'abcd', fileID: 'fileID' });
			await message.save();

			expect(message.createdAt).to.not.equal(undefined);
		});

		it('updatedAt timestamp presence check', async function () {
			const message = await new Message({ message: 'abcd', fileID: 'fileID' });
			await message.save();

			expect(message.updatedAt).to.not.equal(undefined);
		});
	});
	
});
