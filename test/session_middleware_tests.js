
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const { User } = require('../build/persistance/User');
const { Session } = require('../build/persistance/Session');
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const { sessionMiddleware } = require('../build/middlewares/sessionMiddleware');

let SALT = null;

describe('Authentication middleware tests', () => {

	before(async function () {
		await Mongoose.connect('mongodb://localhost:27017/web2Project-test', { useNewUrlParser: true });
		SALT = await bcrypt.genSalt(10);
	});

	beforeEach(async function () {
		await User.deleteMany({});
		await Session.deleteMany({});
	});

	describe('check public routes availablity', function () {
		it('cookie not required - cookie present', async () => {
			const request = httpMocks.createRequest({ signedCookies: { session: "abcd" } });
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware(false)(request, response, next);

			expect(response.statusCode).to.equal(200);
		});

		it('cookie not required - cookie not present', async () => {
			const request = httpMocks.createRequest();
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware(false)(request, response, next);

			expect(response.statusCode).to.equal(200);
		});
	});

	describe('invalid session id tests', function () {
		it('check user is populated in locals', async () => {

			const password = await bcrypt.hash("123456", SALT);

			const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: password });
			await user.save();

			let sessionID = null;

			while (true) {
				sessionID = crypto.randomBytes(32).toString('hex');
				const session_exist = await Session.findOne({ sessionID });
				if (!session_exist) break;
			}

			const session = await new Session({ userID: user._id, sessionID });
			await session.save();

			const request = httpMocks.createRequest({ signedCookies: { session: sessionID } });
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware()(request, response, next);

			expect(response.locals.user.email).to.equal('hello@domain.com');

		});

		it('check correct user is populated', async () => {

			const password = await bcrypt.hash("123456", SALT);

			const user = await new User({ firstName: 'FIRSTNAME', lastName: 'LASTNAME', email: 'hello@domain.com', password: password });
			await user.save();

			let sessionID = null;

			while (true) {
				sessionID = crypto.randomBytes(32).toString('hex');
				const session_exist = await Session.findOne({ sessionID });
				if (!session_exist) break;
			}

			const session = await new Session({ userID: user._id, sessionID });
			await session.save();

			const request = httpMocks.createRequest({ signedCookies: { session: sessionID } });
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware()(request, response, next);

			expect(response.locals.user.email).to.equal('hello@domain.com');

		});

	});

	describe('invalid session id tests', function () {

		it('blocks an invalid user', async () => {

			const request = httpMocks.createRequest({ signedCookies: { session: "abcd" } });
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware(request, response, next);

			expect(response.locals.user).to.equal(undefined);
		});

		it('check response status code 403 - no cookie', async () => {

			const request = httpMocks.createRequest();
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware(true)(request, response, next);

			expect(response.statusCode).to.equal(403);
		});

		it('check response status code 403 - wrong cookie', async () => {

			const request = httpMocks.createRequest({ signedCookies: { session: "abcd" } });
			const response = httpMocks.createResponse();
			const next = sinon.spy();

			await sessionMiddleware(true)(request, response, next);

			expect(response.statusCode).to.equal(403);
		});

	});
	
});
