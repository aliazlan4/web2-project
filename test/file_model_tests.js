
const chai = require('chai');
const expect = chai.expect;
const Mongoose = require('mongoose');
const {File} = require('../build/persistance/File');

describe('File model tests', function() {

    before(async function(){
		await Mongoose.connect('mongodb://localhost:27017/web2Project-test', { useNewUrlParser: true });
    });

    beforeEach(async function() {
        await File.deleteMany({});
    });

    describe('File Name tests', function() {
        it('Check file name length', async function() {
			try {
				const file = await new File({ fileName: '', ipfsID: 'asdfasdf', password_sharing: false, userID: 'sadf' });
				await file.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('files validation failed: fileName: Path `fileName` is required.');
			}
        });
    });

    describe('ipds ID tests', function() {
        it('Check ipfsID name length', async function() {
			try {
				const file = await new File({ fileName: 'FileName', ipfsID: '', password_sharing: false, userID: 'sadf' });
				await file.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('files validation failed: ipfsID: Path `ipfsID` is required.');
			}
        });
    });
	
	describe('password_sharing tests', function() {
        it('Check ipfsID type', async function() {
			try {
				const file = await new File({ fileName: 'FileName', ipfsID: 'sdafasdf', password_sharing: "wrong data", userID: 'sadf' });
				await file.save();
				expect.fail("Expected error not thrown");
			} catch (error) {
				expect(error.message).to.equal('files validation failed: password_sharing: Cast to Boolean failed for value "wrong data" at path "password_sharing"');
			}
        });
    });

	describe('Timestamps presensce test', function () {
		it('createdAt timestamp presence check', async function () {
			const file = await new File({ fileName: 'FileName', ipfsID: 'asdfasdf', password_sharing: false, userID: 'sadf' });
			await file.save();

			expect(file.createdAt).to.not.equal(undefined);
		});

		it('updatedAt timestamp presence check', async function () {
			const file = await new File({ fileName: 'FileName', ipfsID: 'asdfasdf', password_sharing: false, userID: 'sadf' });
			await file.save();

			expect(file.updatedAt).to.not.equal(undefined);
		});
	});
	
});
