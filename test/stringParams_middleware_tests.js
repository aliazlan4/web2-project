
const chai = require('chai');
const expect = chai.expect;
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');

const { strongParamsMiddleware } = require('../build/middlewares/strongParamsMiddleware');

describe('strongParams middleware tests', function() {

	it('check correct field presence', async function () {
		const parameters = { username: "test" };
		const validations = { username: "string" };

		const request = httpMocks.createRequest({ body: parameters });
		const response = httpMocks.createResponse();
		const next = sinon.spy();

		await strongParamsMiddleware(validations)(request, response, next);

		expect(response.locals.strongParams.username).to.equal(parameters.username);
	});

	it('check wrong field type', async function () {
		const parameters = { username: "test" };
		const validations = { username: "boolean" };

		const request = httpMocks.createRequest({ body: parameters });
		const response = httpMocks.createResponse();
		const next = sinon.spy();

		await strongParamsMiddleware(validations)(request, response, next);

		expect(response.locals.strongParams.username).to.equal(undefined);
	});

	it('check field not present in validation', async function () {
		const parameters = { username: "test" };
		const validations = {};

		const request = httpMocks.createRequest({ body: parameters });
		const response = httpMocks.createResponse();
		const next = sinon.spy();

		await strongParamsMiddleware(validations)(request, response, next);

		expect(response.locals.strongParams.username).to.equal(undefined);
	});

	it('check empty string validation', async function () {
		const parameters = { username: "" };
		const validations = { username: "string" };

		const request = httpMocks.createRequest({ body: parameters });
		const response = httpMocks.createResponse();
		const next = sinon.spy();

		await strongParamsMiddleware(validations)(request, response, next);

		expect(response.locals.strongParams.username).to.equal(parameters.username);
	});
	
});
