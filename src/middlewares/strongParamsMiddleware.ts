import { Request, Response, NextFunction, RequestHandler } from "express";

export function strongParamsMiddleware(params: any): RequestHandler {
	return function (request: Request, response: Response, next: NextFunction) {
		const strongParams: {[s: string]: string} = {};

		for (const key in params) {
			const type = params[key];

			if (request.query && request.query[key] !== undefined) {

				if (typeof request.query[key] === type) {

					strongParams[key] = request.query[key];

				}

			}

			else if (request.body && request.body[key] !== undefined) {

				if (typeof request.body[key] === type) {

					strongParams[key] = request.body[key];

				}

			}
		}

		request.body = null;
		request.query = null;

		response.locals.strongParams = strongParams;

		return next();
	}
}