import { Request, Response, NextFunction, RequestHandler } from "express";
import { Session } from "../persistance/Session";
import { User } from "../persistance/User";

export function sessionMiddleware(requireAuth = true): RequestHandler {
	return async function (request: Request, response: Response, next: NextFunction) {
		if (request.signedCookies?.session) {
			const sessionID: string = request.signedCookies.session;

			const session = await Session.findOne({ sessionID });
			
			if (!session && requireAuth) return response.sendStatus(403);
			else if (!session) return next();

			response.locals.user = await User.findById(session?.userID);

			return next();
		} else {
			if (requireAuth) {
				response.sendStatus(403);
				return next(new Error('Cookie was required for request but no cookie was found'));
			}

			return next();
		}

	}
}