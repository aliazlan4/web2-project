import Mongoose from "mongoose";
import Express, { Application, Request, Response } from 'express';
import cookieParser from "cookie-parser";
import WebSocket from "ws";
import http from "http";
import cors from "cors";
import { sessionMiddleware } from "./middlewares/sessionMiddlewares";
import { strongParamsMiddleware } from "./middlewares/strongParamsMiddleware";

import * as UserService from "./services/User";
import * as FileService from "./services/File";
import * as SessionService from "./services/Session";
import * as MessageService from "./services/Message";

const config = {
	port: 5000,
	db_name: "web2Project",
	session_secret: 'I am a secret!'
};

(async () => {

	await Mongoose.connect('mongodb://localhost:27017/' + config.db_name, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	const app: Application = Express();

	app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
	app.use(cookieParser(config.session_secret));
	app.use(Express.json());
	

	///// APP ROUTES /////
	app.post("/api/login", [sessionMiddleware(false), strongParamsMiddleware({ email: "string", password: "string" })], UserService.login);
	app.post("/api/register", [sessionMiddleware(false), strongParamsMiddleware({ firstName: "string", lastName: "string", email: "string", password: "string" })], UserService.register);

	app.get("/api/user", sessionMiddleware(), SessionService.getSessionUser);
	app.post("/api/user/passwordReset", [sessionMiddleware(false), strongParamsMiddleware({ password: "string" })], UserService.passwordReset);

	app.get("/api/myFiles", [sessionMiddleware(), strongParamsMiddleware({})], FileService.myFiles);
	app.get("/api/getFile", [sessionMiddleware(), strongParamsMiddleware({ id: "string" })], FileService.getFile);
	app.post("/api/addFile", [sessionMiddleware(), strongParamsMiddleware({ fileName: "string", ipfsID: "string", password_sharing: "boolean", password: "string" })], FileService.addFile);
	///// APP ROUTES /////

	const server = http.createServer(app);
	const wss = new WebSocket.Server({ server });

	wss.on('connection', (webSocketClient: WebSocket) => {

		webSocketClient.on('message', (message: string) => {

			const request = JSON.parse(message);
			MessageService.handleRequest(webSocketClient, request.type, request.data);
		});

		webSocketClient.on('close', function () {
			
			MessageService.handleRequest(webSocketClient, "client_disconnected");
		});

	});

	server.listen(config.port, function () {
		console.log(`server started at http://localhost:${config.port}`);
	});

})();