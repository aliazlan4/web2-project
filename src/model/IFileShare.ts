import { Document } from 'mongoose';

export interface IFileShare extends Document {
    fileID: string,
    userID: string,
	createdAt: string,
	updatedAt: string
}