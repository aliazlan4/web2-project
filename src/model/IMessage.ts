import { Document } from 'mongoose';

export interface IMessage extends Document {
    fileID: string,
	message: string,
	createdAt: string,
	updatedAt: string
}