import { Document } from 'mongoose';

export interface IFile extends Document {
    fileName: string,
    ipfsID: string,
	password_sharing: boolean,
	password: string,
	userID: string,
	createdAt: string,
	updatedAt: string
}