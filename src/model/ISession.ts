import { Document } from 'mongoose';

export interface ISession extends Document {
	session_id: string,
	userID: string,
	createdAt: string,
	updatedAt: string
}