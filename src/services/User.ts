import { User } from "../persistance/User";
import { Session } from "../persistance/Session";
import { Request, Response } from "express";
import bcrypt from "bcrypt";
import crypto from "crypto";

const SALT_WORK_FACTOR = 10;

export async function login(request: Request, response: Response): Promise<any> {
	const { email, password } = response.locals.strongParams;
	
	const user = await User.findOne({ email: email });

	if (!user || !(await bcrypt.compare(password, user!.password)))
		return response.json({ status: false, message: "user doesnt exist or password incorrect" });

	await createSessionAndSetCookie(user._id, response);

	delete user.password;

	return response.json({ status: true, message: "user logged in successfully", user });
}

export async function register(request: Request, response: Response): Promise<any> {
	let { email, password, firstName, lastName } = response.locals.strongParams;

	const user_exist = await User.findOne({ email: email });
	if (user_exist) return response.json({ status: false, message: "email already exists"});

	const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
	password = await bcrypt.hash(password, salt);

	const user = await new User({ email, password, firstName, lastName });
	await user.save();

	await createSessionAndSetCookie(user._id, response);

	delete user.password;

	return response.json({ status: true, message: "user registered successfully", user});
}

export async function passwordReset(request: Request, response: Response): Promise<any> {
	const { password } = response.locals.strongParams;
	
	const user = await User.findById(response.locals.user._id);

	const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
	user!.password = await bcrypt.hash(password, salt);

	await user!.save();

	return response.json({ status: true, message: "password changed successfully"});
}

async function createSessionAndSetCookie(userID: String, response: Response) {
	let sessionID = null;

	while (true) {
		sessionID = crypto.randomBytes(32).toString('hex');
		const session_exist = await Session.findOne({ sessionID });
		if (!session_exist) break;
	}

	const session = await new Session({ userID, sessionID });
	await session.save();

	response.cookie('session', sessionID, { signed: true });
}