import { Message } from "../persistance/Message";
import { Request, Response } from "express";
import WebSocket from "ws";

const fileToSocketMap = new Map();
const socketToFileMap = new Map();

interface Data {
	message?: String,
	fileID?: String
}

export async function handleRequest(client: WebSocket, type: String, data?: Data) {
	switch (type) {
		case "client_disconnected":
			await clientDisconnected(client); break;
		case "subscribe":
			await subscribe(client, data!); break;
		case "message":
			await addMessage(client, data!); break;
	}
}

export async function fileMessages(request: Request, response: Response): Promise<any> {
	const { fileID } = response.locals.strongParams;

	const messages = await Message.find({ fileID: fileID });

	return response.json({ status: true, messages });
}

export async function addMessage(client: WebSocket, data: Data) {
	const { message, fileID } = data;

	const clients = fileToSocketMap.get(data.fileID);
	for (const peer of clients) {
		peer.send({
			type: "message",
			data: { message }
		});
	}

	const new_message = await new Message({
		message: message,
		fileID: fileID,
	});
	await new_message.save();
}

async function subscribe(client: WebSocket, data: Data) {
	if (!fileToSocketMap.get(data.fileID)) fileToSocketMap.set(data.fileID, [ client ]);
	else {
		const clients = fileToSocketMap.get(data.fileID);
		clients.push(client);
		fileToSocketMap.set(data.fileID, clients);
	}

	socketToFileMap.set(client, data.fileID);

	const messages = await Message.find({ fileID: data.fileID });
	client.send({
		type: "all_messages",
		data: { messages }
	});
}

async function clientDisconnected(client: WebSocket) {
	const file = socketToFileMap.get(client);
	socketToFileMap.set(client, undefined);
	const clients = fileToSocketMap.get(file);
	clients.splice(clients.indexOf(client), 1);
	fileToSocketMap.set(file, clients);
}