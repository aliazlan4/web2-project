import { File } from "../persistance/File";
import { Request, Response } from "express";

export async function myFiles(request: Request, response: Response): Promise<any> {
	const files = await File.find({ userID: response.locals.user._id });

	return response.json({ status: true, files });
}

export async function addFile(request: Request, response: Response): Promise<any> {
	const { fileName, ipfsID, password_sharing, password } = response.locals.strongParams;

	const file = await new File({
		fileName: fileName,
		ipfsID: ipfsID,
		password_sharing: password_sharing,
		password: password,
		userID: response.locals.user._id,
	});
	await file.save();

	return response.json({ status: true, file });
}

export async function getFile(request: Request, response: Response): Promise<any> {
	const { id } = response.locals.strongParams;

	const file = await File.findById(id);

	if (!file || file.userID != response.locals.user.id)
		return response.json({ status: false, message: "file not found" });

	return response.json({ status: true, file });
}