import { Session } from "../persistance/Session";
import { Request, Response } from "express";

export async function getSessionUser(request: Request, response: Response): Promise<void> {
	const { firstName, lastName, email } = response.locals.user;

	response.json({ status: true, user: { firstName, lastName, email } });
}