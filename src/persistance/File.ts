import Mongoose from 'mongoose';
import {IFile} from "../model/IFile";

const fileSchema: Mongoose.Schema<IFile> = new Mongoose.Schema<IFile>({

    fileName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
            message: 'File name is not valid'
        }
    },

    ipfsID: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
				return value.length > 0;
            },
			message: 'IPFS id is not valid'
        }
    },

	password_sharing: {
        type: Boolean,
		required: true,
		default: false,
		validate: {
            validator: function (value: Boolean): boolean {
                return value === true || value === false;
            },
            message: 'Password sharing is not valid'
        }
    },

	password: {
        type: String,
		required: false,
		validate: {
            validator: function (value: String): boolean {
                return value.length > 0;
            },
            message: 'Password is not valid'
        }
    },

	userID: {
        type: String,
		required: true,
		validate: {
            validator: function (value: String): boolean {
                return value.length > 0;
            },
			message: 'User id is not valid'
        }
    },

}, { timestamps: true });

export const File = Mongoose.model<IFile>('files', fileSchema);