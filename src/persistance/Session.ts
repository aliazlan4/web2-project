import Mongoose from 'mongoose';
import {ISession} from "../model/ISession";

const sessionSchema: Mongoose.Schema<ISession> = new Mongoose.Schema<ISession>({

    sessionID: {
        type: String,
        required: true,
		unique: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length === 64;
            },
            message: 'Session id is invalid'
        }
    },

    userID: {
        type: String,
        required: true,
        validate: {
            validator: function (value: String): boolean {
                return value.length > 0;
            },
            message: 'User id is not valid'
        }
    },
	
}, { timestamps: true });

export const Session = Mongoose.model<ISession>('sessions', sessionSchema);