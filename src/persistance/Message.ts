import Mongoose from 'mongoose';
import {IMessage} from "../model/IMessage";

const messageSchema: Mongoose.Schema<IMessage> = new Mongoose.Schema<IMessage>({
    fileID: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
            message: 'File id is not valid'
        }
    },

    message: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
            message: 'Message is not valid'
        }
    },

}, { timestamps: true });

export const Message = Mongoose.model<IMessage>('messages', messageSchema);