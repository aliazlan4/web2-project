import Mongoose from 'mongoose';
import { IUser } from "../model/IUser";


const userSchema: Mongoose.Schema<IUser> = new Mongoose.Schema<IUser>({

    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 2 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name must have more than two characters and may only contain letters'
        }
    },

    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
				return value.length > 2 && /^[a-zA-Z]*$/.test(value);
            },
			message: 'Last name must have more than two characters and may only contain letters'
        }
    },

	email: {
        type: String,
		required: true,
		unique: true,
		validate: {
            validator: function (value: String): boolean {
                return value.length > 0 && value.includes("@");
            },
            message: 'Email is not valid'
        }
    },

	password: {
        type: String,
		required: true,
		validate: {
            validator: function (value: String): boolean {
                return value.indexOf("$2") === 0;
            },
            message: 'Password is not valid'
        }
    },
	
}, { timestamps: true });

userSchema.virtual('fullName')
    .get(function (this: IUser) {
        return this.firstName + ' ' + this.lastName;
    }).set(function (this: IUser, fullName: string) {
        const [firstName, lastName]: string[] = fullName.split(' ');
        this.firstName = firstName;
        this.lastName = lastName;
    }
);

export const User = Mongoose.model<IUser>('users', userSchema);