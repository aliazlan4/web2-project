import Mongoose from 'mongoose';
import {IFileShare} from "../model/IFileShare";

const fileShareSchema: Mongoose.Schema<IFileShare> = new Mongoose.Schema<IFileShare>({

	fileID: {
        type: String,
        required: true,
        validate: {
            validator: function (value: string): boolean {
                return value.length > 0;
            },
            message: 'File id is not valid'
        }
    },

	userID: {
        type: String,
		required: true,
		validate: {
            validator: function (value: String): boolean {
                return value.length > 0;
            },
			message: 'User id is not valid'
        }
    },

}, { timestamps: true });

export const FileShare = Mongoose.model<IFileShare>('file_shares', fileShareSchema);